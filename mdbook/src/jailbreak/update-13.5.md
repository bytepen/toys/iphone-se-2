# Update to iOS 13.5

## Summary

unc0ver 5.0.1 is designed for iOS 13.5, but as of June 1st, 2020, iOS 13.5.1 is the most current version, which means that updating to 13.5 isn't as straight forward as it could be, but still isn't hard. I'll be using Windows 10 for this as I need iTunes. When I got my iPhone SE 2 (2020) on June 4th, 2020, it was on iOS 13.4.1, so I needed to upgrade it to iOS 13.5.

![iOS 13.4.1](../images/ios-13.4.1.png)

## Required Software

You will need to download and install the following software:

- iTunes (NOT UWP version. If you installed from the Microsoft Store, uninstall it first)
  - [64-Bit](https://www.apple.com/itunes/download/win64)
  - [32-Bit](https://www.apple.com/itunes/download/win32)

I won't walk through installing iTunes since it's pretty standard affair. Make sure you can run it and it sees your iPhone.

You will need to download the iOS 13.5 IPSW file from [ipsw.me](https://ipsw.me).

They're different for each device, but you can find the one I used below:

- [iPhone SE 2 (2020) iOS 13.5 IPSW](https://api.ipsw.me/v4/ipsw/download/iPhone12,8/17F75)

## Update

Go to the iDevice Summary page. You'll see you are prompted to install iOS 13.5.1. You can select a specific firmware by holding `Shift` and clicking `Update`. Select the iOS 13.5 IPSW we downloaded above and the process of updating to 13.5 will begin.

![iTunes - iOS 13.4.1](../images/itunes-13.4.1.png)
![Select iOS](../images/itunes-select-13.5.png)
![Update to 13.5](../images/update-to-13.5.png)

Once finished, go to iOS `Settings` => `General` => `About` and confirm you are on iOS 13.5.

![iOS 13.5](../images/ios-13.5.png)
