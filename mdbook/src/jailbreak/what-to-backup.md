# Items to Backup

## Summary

So far we have jailbroken iOS 13.5 with unc0ver 5.0.1 and dumped the .SHSH2 blobs. I also wanted to briefly touch on what to do if things go super wrong and you need to restore your device to iOS 13.5 well after Apple has stopped signing it. Unfortunately, I couldn't find great instructions and it appears that newer devices may not be able to do much at the current time without a jailbreak already in place. I'll look more into it when I cross that bridge. In the meantime, here's a list of information that you should definitely make sure you have backed up.

You will need to download the iOS 13.5 IPSW file from [ipsw.me](https://ipsw.me).

- [iPhone SE 2 (2020) iOS 13.5 IPSW](https://api.ipsw.me/v4/ipsw/download/iPhone12,8/17F75)
 
You will need the following information:

- ECID: Found in iTunes by connecting the device and clicking the Serial Number (Ex: 4485E2859345B)
- Model Identifier: Found just like ECID (Ex: iPhone12,8)
- Your SHSH2 Blob
- Your Nonce Generator

## Get your ECID and Model Identifier

Open iTunes and connect your device. Go to the devices page and click the `Serial Number`. This will cycle through your `Serial Number`, `UDID`, `ECID`, and `Model Identifier`. It probably wouldn't be a bad idea to just keep all of this information in your notes.

## Get your SHSH2 Blob

You can follow my previous step on getting the .SHSH2 Blob to get yours.

## Get your Nonce Generator

We will first need to get the Nonce Generator Key associated with your backup. The best place to get this is from within the .SHSH2 blob itself. Look for the following lines around `line 166`.

```
<key>generator</key>
<string>0x[YourGenerator]</string>
```

## What now?

With all of this information securely backed up, let's say your iDevice accidentally gets updated or is unable to boot. It is possible that, even well after Apple has stopped signing your iOS version, that you can restore your device back to the desired iOS version. The steps appear to vary based on device, and it seems like there isn't great information for A13 devices yet, but most of them use 3rd party tools like [FutureRestore](https://github.com/tihmstar/futurerestore) alongside the information on this page to allow you to restore.
