# Backup .SHSH2 Blob

## Summary

Typically, you can only install an iOS version while Apple is signing it. The .SHSH2 blob is a file that can be used to restore/downgrade to a specific iOS version well after Apple has stopped signing it. .SHSH2 blobs are specific to YOUR iDevice and can be generated for a version of iOS only while Apple is still signing it, so time is of the essence. As of June 4th, 2020, Apple is still signing iOS 13.5, so you are still able to generate the .SHSH2 blobs for your device so you can make sure you're always able to have a jailbroken iOS 13.5 iDevice.

## Install System Info

There is a tweak called `System Info` that is available in a Cydia repository after adding said repository. This tweak shows you all the information you could possible need for this step and more.

Open the Cydia app on your phone and go to the `Sources` menu item. Click `Edit` in the top right corner and `Add` in the top left corner. Type `https://apt.xninja.xyz` and click `Add Source`.

![Cydia - ARX8x Added](../images/cydia-add-arx8x.png)

Once added, go back to `Sources` and click on `ARX8x's repo` => `All Packages` => `System Info`. Click `Modify` in the top right corner and click `Install`.

![Cydia - System Info](../images/cydia-system-info.png)

## Backup .SHSH2 Blob

I find the easiest way to back up your .SHSH2 blob is via email, but these steps will save it to your device so you can back it up however you like. If you're doing email, make sure it is set up before continuing.

Open iOS `Settings` => `About` and navigate to the `System Info` section.

![About - System Info](../images/about-system-info.png)

Locate the `ECID` line and swipe it from `Right` to `Left`. Click `Save SHSH2`. Select `13.5 - 17F75` from the options to back up your iOS 13.5 .SHSH2 Blob.

![About - Save SHSH2](../images/about-save-shsh2.png)
![SHSH2 - Select Version](../images/shsh2-select-version.png)

You may be prompted with `405 - no pairs found for ECID`. Click `Derive New`. If it says `ApNonce Pair`, you should be fine to `Use` the old pair, but you can `Derive New` if you want. Either way, you should definitely make the `ApNonce Pair` window appear and save it in your backups in case you need to use your SHSH2 blob to restore in the future.

![SHSH2 - 405 - No Pairs](../images/shsh2-405-no-pairs.png)
![SHSH2 - ApNonce Pair](../images/shsh2-apnonce-pair.png)

It will save your .SHSH2 blob to `/private/var/mobile/SHSH/13.5/*.shsh2/`. Click the `Share` button and email it to yourself. Note the the name of the file is in the following format:

```
<Generator in Decimal Form>_<Model Identifier>_<ApNonce>.shsh2
```

I would highly recommend leaving it named this way so that you have this information easily available when you need it.

While you're here, it couldn't hurt to have a screenshot of the entire System Info page for your backups.

## Verify .SHSH2 Blob

With your .SHSH2 Blob backed up on your computer, you can got to the [Blob Checker](https://tsssaver.1conan.com/check/) and upload your Blob. The Identifier and Version should fill automatically, but verify they are correct and that you are not a robot and click `Submit`.

![SHSH2 Blob Checker](../images/shsh2-check.png)

You should recieve the message `SHSH2 is valid!` and you should be good to go. It doesn't hurt to keep a copy of all the text on this page and make sure your .SHSH2 is safely backed up alongside this information and the screenshots of your `System Info`.

![SHSH2 Blob Check - Result](../images/shsh2-check-result.png)
