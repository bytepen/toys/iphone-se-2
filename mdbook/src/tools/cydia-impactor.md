# Cydia Impactor

## Summary

[Cydia Impactor](http://www.cydiaimpactor.com/) is a tool that makes installing unsigned IPAs on your iPhone a breeze. It's avaliable on macOS, Linux, and Windows, however, for this tutorial, I will be focusing primarily on Windows as that's what I have installed this week.

The biggest downside to this is that it requires an Apple Developer ID, which costs $100 per year. That's no small change, but if you're working with the iPhone on a regular basis, it's well worth the cost to not have to deal with things like unsigned IPA's being difficult to install, having to re-sign everything every 7 days and the hassle of forgetting to re-sign AltStore, or having a limit of 10 apps you can sign per week. I can say with certainty that I will make up the $100 (and then some) in the time that I would have lost without this.

Apparently there's a way around needing a developer account with a Cydia Substrate tweak, but I believe I'd still have issues considering I'm on iOS 13.5 with unc0ver and that isn't a permanent jailbreak. If $100 per year is too steep for you, I recommending figuring out how to go about that.

## Installation & Setup

### Purchase an [Apple Developer Account](https://developer.apple.com/)

I don't know how the exact process works these days since I set mine up in like 2007 and was just able to renew it, but it should be pretty self explanitory.

You will then need to create an App-Specific Password to use with Cydia Impactor. Depending on how much is linked to your Apple account, it might not be a terrible idea to create a second account and it as a Developer to your main account, then create an App-Specific Password for that new account. That way, if it gets leaked, you aren't giving out access to your main account. I do not know if this is possible with personal accounts, though.

### "Install" Cydia Impactor

You can download Cydia Impactor [here](http://www.cydiaimpactor.com/) and extract it. It's a 32-bit program with no proper installer, so I opted to move the folder to `C:\Program Files(x86)\Impactor` and create a shortcut in `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Impactor` called `Impactor` so I could easily launch it in the future.


### Setup Cydia Impactor

I'll take the time to note here that the application is not resizeable and looks terrible on my 4k monitor due to resizing issues seen below.

![Cydia Impactor - Bad Size](../images/cydia-impactor-bad-size.png)

To fix this, I right clicked on `Impactor.exe` and clicked on `Properties` => `Compatibility`, `Change high DPI settings` and changed `Override high DPI scaling behavior` to `System (Enhanced)`. Much better!

![Cydia Impactor - Good Size](../images/cydia-impactor-good-size.png)

Now that everything's pretty and installed, we can move on to setting up a few QoL things.

Cydia Impactor will, by default, ask you for your username and app-specific password every time you  try to install an app. This can get annoying pretty quickly if you install a lot of apps. To get around that, you can hit the `Super (Windows) Key` and type `env`. Press Enter to open `Edit the system environment variables`. Click on `Environment Variables` and add two new entries to the top pane.

```
IMPACTOR_APPLEID_USERNAME
IMPACTOR_APPLEID_PASSWORD
```

These should be fairly obvious, so put in your username and app-specific password here.

### Usage

Once you have all of the above set up, you should be able to just connect your iPhone via USB, make sure it is selected from the list, and drag the IPA onto the Window. Within moments, your IPA should be installed on your device.

If you're looking to get started with something, I'd check out the [OWASP MSTG Crackme's](https://github.com/OWASP/owasp-mstg/tree/master/Crackmes).


NOTE: I've been having some issues with Cydia Impactor recently that are causing an error whenever the ipa I'm trying to upload is being validated. I've been meaning to look into this more, but for the meantime, I've been using AltStore with my Developer Account to get unlimited apps that don't expire in one week.
